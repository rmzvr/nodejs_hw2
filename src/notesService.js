const { Note } = require('./models/Note')

const getUserNotes = async (req, res, next) => {
  const userID = req.user._id
  const offset = +req.query.offset
  const limit = +req.query.limit

  try {
    const notes = await Note.find({ userId: userID }, '-__v')

    res.status(200).json({
      offset: offset || 0,
      limit: limit || 0,
      count: notes.length,
      notes: notes.slice(offset, limit || notes.length)
    })
  } catch (error) {
    next(error.message)
  }
}

const addUserNote = async (req, res) => {
  const noteContent = req.body.text

  const note = new Note({
    userId: req.user._id,
    text: noteContent
  })

  try {
    await note.save()

    res.status(200).json({ message: 'Success' })
  } catch (error) {
    res.status(400).json({ message: error.message })
  }
}

const getUserNoteByID = async (req, res) => {
  const userID = req.user._id
  const noteID = req.params.id

  try {
    const note = await Note.findOne({ userId: userID, _id: noteID }, '-__v')

    res.status(200).json({ note: note })
  } catch (error) {
    res.status(400).json({ message: error.message }) 
  }
}

const updateUserNoteByID = async (req, res) => {
  const userID = req.user._id
  const noteID = req.params.id
  const noteContent = req.body.text

  try {
    await Note.findByIdAndUpdate(
      { userId: userID, _id: noteID },
      { text: noteContent }
    )
    
    res.status(200).json({ message: 'Success' })
  } catch (error) {
    res.status(400).json({ message: error.message })
  }
}

const toggleCompletedForUserNoteByID = async (req, res) => {
  const userID = req.user._id
  const noteID = req.params.id

  try {
    const book = await Note.findOne({ userId: userID, _id: noteID })

    book.completed = !book.completed

    book.save()

    res.status(200).json({ message: 'Success' })
  } catch (error) {
    res.status(400).json({ message: error.message })
  }
}

const deleteUserNoteByID = async (req, res) => {
  const userID = req.user._id
  const noteID = req.params.id

  try {
    await Note.findByIdAndDelete({ userId: userID, _id: noteID })

    res.status(200).json({ message: 'Success' })
  } catch (error) {
    res.status(400).json({ message: error.message })
  }
}

module.exports = {
  getUserNotes,
  addUserNote,
  getUserNoteByID,
  updateUserNoteByID,
  toggleCompletedForUserNoteByID,
  deleteUserNoteByID
}
