const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

const { User } = require('./models/User')

const registerUser = async (req, res) => {
  try {
    const { username, password } = req.body

    if (!username) {
      throw Error('Username is required')
    }

    if (!password) {
      throw Error('Username is required')
    }

    const user = new User({
      username,
      password: await bcrypt.hash(password, 10)
    })

    user
      .save()
      .then(() => {
        res.status(200).json({ message: 'Success' })
      })
  } catch (error) {
    return res.status(400).json({ message: error.message })
  }
}

const loginUser = async (req, res) => {
  try {
    const username = req.body.username
    const password = req.body.password

    const user = await User.findOne({ username: username })

    if (!user) {
      throw Error('Not authorized')
    }

    const isPasswordCorrect = await bcrypt.compare(
      String(password),
      String(user.password)
    )

    if (!isPasswordCorrect) {
      throw Error('Not authorized')
    }

    const payload = {
      username: user.username,
      userID: user._id,
      createdDate: user.createdDate
    }
    const jwtToken = jwt.sign(payload, process.env.SECRET_JWT_KEY)

    return res.status(200).json({
      message: 'Success',
      jwt_token: jwtToken
    })
  } catch (error) {
    return res.status(400).json({ message: error.message })
  }
}

module.exports = {
  registerUser,
  loginUser
}
