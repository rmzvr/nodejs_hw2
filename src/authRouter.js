const express = require('express')
const router = express.Router()

const { registerUser, loginUser } = require('./authService.js')

router.post('/register', registerUser)

router.post('/login', loginUser)

module.exports = {
  authRouter: router
}
