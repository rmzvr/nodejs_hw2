const passwordMiddleware = async (req, res, next) => {
  try {
    const { _id } = req.user
    const { oldPassword, newPassword } = req.body

    req.user = {
      _id,
      oldPassword,
      newPassword
    }

    next()
  } catch (err) {
    return res.status(401).json({ message: err.message })
  }
}

module.exports = {
  passwordMiddleware
}
