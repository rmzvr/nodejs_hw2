const express = require('express')
const router = express.Router()
const {
  getUserNotes,
  addUserNote,
  getUserNoteByID,
  updateUserNoteByID,
  toggleCompletedForUserNoteByID,
  deleteUserNoteByID
} = require('./notesService')

router.get('/', getUserNotes)

router.post('/', addUserNote)

router.get('/:id', getUserNoteByID)

router.put('/:id', updateUserNoteByID)

router.patch('/:id', toggleCompletedForUserNoteByID)

router.delete('/:id', deleteUserNoteByID)

module.exports = {
  notesRouter: router
}
